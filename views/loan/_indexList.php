<?php
use yii\helpers\Html;
?>
<tbody>
    <tr>
        <td><?= Html::encode($model['id']) ?></td>
        <td><?= Html::encode($model['user_id']) ?></td>
        <td><?= Html::encode($model['amount']) ?></td>
        <td><?= Html::encode($model['interest']) ?></td>
        <td><?= Html::encode($model['duration']) ?></td>
        <td><?= Html::encode($model['start_date']) ?></td>
        <td><?= Html::encode($model['end_date']) ?></td>
        <td><?= Html::encode($model['campaign']) ?></td>
        <td><?= Html::encode($model['status']) ?></td>
        <td><?= Html::a('View', ["/loan/view/{$model['id']}"], ['class'=>'btn btn-primary']) ?> <?= Html::a('Edit', ["/loan/edit/{$model['id']}"], ['class'=>'btn btn-warning']) ?> <?= Html::a('Delete', ["/loan/delete/{$model['id']}"], ['class'=>'btn btn-danger']) ?></td>
    </tr>
</tbody>
