<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'List of loan';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="table-container">
  <div class="row">
    <div class="col-lg-12">
      
      <div class="row">
          <div class="col-lg-12">
            <h1> Front-end style should be base on this page. </h1>
          </div>
      </div>

      <div class="row">
        
        <div class="col-lg-12">
            <?= Html::a('<span>Add Loan</span>', ["/loan/add"], ['class'=>'btn btn-primary']) ?>
        </div>

      </div>

      <div class="row">
        <div class="col-lg-12">

            <?php Pjax::begin(); ?>
            <?=
            GridView::widget([
                'tableOptions' => [
                    'class' => 'table table-hover table-sm',
                  ],
                'dataProvider' => $listDataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'amount',
                    'interest', 
                    'duration',
                    'start_date:datetime',
                    'end_date:datetime',
                    'campaign',
                    'status:boolean',
                    [
                    'attribute' => 'User Name',
                    'format' => 'raw',
                    'value' => function ($loan) {
                        $full_name = $loan->user->first_name . ' ' . $loan->user->last_name;
                        return Html::a($full_name, ['user/view', 'id' => $loan->user->getAttribute('id')]);
                    },
                    ],
                    [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadAdd} {leadView} {leadEdit} {leadDelete}',
                    'buttons' => [
                            'leadView' => function ($url, $loan) {
                                $url = Url::to(['loan/view', 'id' => $loan->getAttribute('id')]);
                                return Html::a('<span>View</span>', $url, ['title' => 'view', 'class' => 'btn btn-primary']);
                            },
                            'leadEdit' => function ($url, $loan) {
                                $url = Url::to(['loan/edit', 'id' => $loan->getAttribute('id')]);
                                return Html::a('<span>Edit</span>', $url, ['title' => 'update', 'class' => 'btn btn-success']);
                            },
                            'leadDelete' => function ($url, $loan) {
                                $url = Url::to(['loan/delete', 'id' => $loan->getAttribute('id')]);
                                return Html::a('<span>Delete</span>', $url, [
                                    'title' => 'delete',
                                    'class' => 'btn btn-danger',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this user?'),
                                    'data-method' => 'post',
                                ]);
                            },
                    ]],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
      </div>

  </div>
</div>
<div class="row">
      <div class="col-lg-12">
        <?= Html::a('<span>Back</span>', '/', ['class' => 'btn btn-default']);?>
      </div>
</div>
