<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Edit loan';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if(isset($error)) {
    if (is_array($error)) { 
        foreach ($error as $value) { ?>
            <div class="alert alert-danger" role="alert">
                <p><?= Html::encode($value[0]) ?></p>
            </div>
        <?php } 
        } else { ?> 
        <div class="alert alert-danger" role="alert">
            <p><?= Html::encode($error) ?></p>
        </div>
<?php   }
    } ?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($loan, 'id')->textInput(['readonly'=> true]) ?>
    <?= $form->field($loan, 'user_id') ?>
    <?= $form->field($loan, 'amount') ?>
    <?= $form->field($loan, 'interest') ?>
    <?= $form->field($loan, 'duration') ?>
    <?= $form->field($loan, 'start_date') ?>
    <?= $form->field($loan, 'end_date') ?>
    <?= $form->field($loan, 'campaign') ?>
    <?= $form->field($loan, 'status')->dropDownList(['0' => 'NO', '1' => 'Yes']) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>