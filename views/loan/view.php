<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'View loan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
  <h2>Loan Veiw</h2>
  <table class="table">
    <thead>
      <tr>
      <th>ID</th>
        <th>User ID</th>
        <th>Amount</th>
        <th>Interest</th>
        <th>Duration</th>
        <th>Start Date</th>
        <th>end Date</th>
        <th>Campaign</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <?= 
    ListView::widget([
        'itemView'  => '_indexList',
        'dataProvider' => $dataProvider,
    ]); 
    ?>
  </table>
</div>
</body>
</html>
