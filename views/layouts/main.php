<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300&display=swap" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <nav class="navbar">
    <div class="container">
      <div class="row">
          <div class="col-md-11">
         </div>  
        <div class="col-md-1">
            <?= Html::a('<span>Login</span>', '#', ['class' => 'btn btn-default']);?>
        </div>
      </div>
    </div>
    </nav>
    <nav class="navbar">
        <div class="container">
            <div class="row">
                <div class="image col-sm-3">
                    <?= Html::img('@web/logo.jpeg', ['class' => 'pull-left img-responsive']);?>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-2">
                            <h3> Add </h3>
                        </div>
                        <div class="col-sm-2">
                            <h3> Here </h3>
                        </div>
                        <div class="col-sm-2">
                            <h3> Random </h3>
                        </div>
                        <div class="col-sm-6">
                            <h3> Links to our page </h3>
                        </div>
                    </div>                
                </div>
                <div class="col-sm-3">
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="breadcrumb">
        <div class="container">
            <div class="row">
                <h4><?= Html::a('Home', ["/"], ['class'=>'col-md-1']) ?></h4>
                <h4><?= Html::a('Users', ["/user/index"], ['class'=>'col-md-1']) ?></h4>
                <h4><?= Html::a('Loans', ["/loan/index"], ['class'=>'col-md-1']) ?></h4>
            </div>
        </div>
    </div>
    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
