<?php
use yii\helpers\Html;
?>
<tbody>
    <tr>
        <td><?= Html::encode($model['id']) ?></td>
        <td><?= Html::encode($model['first_name']) ?></td>
        <td><?= Html::encode($model['last_name']) ?></td>
        <td><?= Html::encode($model['email']) ?></td>
        <td><?= Html::encode($model['personal_code']) ?></td>
        <td><?= Html::encode($model['phone']) ?></td>
        <td><?= Html::encode($model['active']) ?></td>
        <td><?= Html::encode($model['dead']) ?></td>
        <td><?= Html::encode($model['lang']) ?></td>
        <td><?= Html::a('View', ["/user/view/{$model['id']}"], ['class'=>'btn btn-primary']) ?> <?= Html::a('Edit', ["/user/edit/{$model['id']}"], ['class'=>'btn btn-warning']) ?> <?= Html::a('Delete', ["/user/delete/{$model['id']}"], ['class'=>'btn btn-danger']) ?></td>
    </tr>
</tbody>
