<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Edit user';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if(isset($error)) {
    if (is_array($error)) { 
        foreach ($error as $value) { ?>
            <div class="alert alert-danger" role="alert">
                <p><?= Html::encode($value[0]) ?></p>
            </div>
        <?php } 
        } else { ?> 
        <div class="alert alert-danger" role="alert">
            <p><?= Html::encode($error) ?></p>
        </div>
<?php   }
    } ?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($user, 'id')->textInput(['readonly'=> true]) ?>
    <?= $form->field($user, 'first_name') ?>
    <?= $form->field($user, 'last_name') ?>
    <?= $form->field($user, 'email') ?>
    <?= $form->field($user, 'personal_code') ?>
    <?= $form->field($user, 'phone') ?>
    <?= $form->field($user, 'active')->dropDownList(['0' => 'NO', '1' => 'Yes']) ?>
    <?= $form->field($user, 'dead')->dropDownList(['0' => 'NO', '1' => 'Yes']) ?>
    <?= $form->field($user, 'lang') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>