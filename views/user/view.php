<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'View user';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
  <h2>User Veiw</h2>
  <table class="table">
    <thead>
      <tr>
        <th>ID</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Personal Code</th>
        <th>Phone</th>
        <th>Active</th>
        <th>Dead</th>
        <th>Lang</th>
        <th>Action</th>
      </tr>
    </thead>
    <?= 
    ListView::widget([
        'itemView'  => '_indexList',
        'dataProvider' => $dataProvider,
    ]); 
    ?>
  </table>
</div>
</body>
</html>
