<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'List of user';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="table-container">
  <div class="row">
    <div class="col-lg-12">
      
      <div class="row">
          <div class="col-lg-12">
            <h1> Front-end style should be base on this page. </h1>
          </div>
      </div>

      <div class="row">
        
        <div class="col-lg-12"><?= Html::a('<span>Add User</span>', ["/user/add"], ['class'=>'btn btn-primary']) ?></div>

      </div>

      <div class="row">
        <div class="col-lg-12">
          <?php Pjax::begin(); ?>
          <?=

            GridView::widget([
              'tableOptions' => [
                'class' => 'table table-hover table-sm',
              ],
              'dataProvider' => $listDataProvider,
              'filterModel' => $searchModel,
              'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],
                  'id',
                  'first_name',
                  'last_name',
                  'email',
                  'personal_code',
                  'phone',
                  'active:boolean',
                  'dead:boolean',
                  'lang',
                  [
                    'attribute' => 'age',
                    'label' => 'Age',
                    'value' => function ($user) {
                        return $user::getUserAge($user->personal_code);
                    },
                  ],
                  [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{leadAdd} {leadView} {leadEdit} {leadDelete}',
                    'buttons' => [
                        'leadAdd' => function ($url, $user) {
                              $url = Url::to(['loan/add', 'user_id' => $user->getAttribute('id')]);
                              return Html::a('<span>Add a loan to user</span>', $url, ['title' => 'view', 'class' => 'btn btn-success']);
                          },
                          'leadView' => function ($url, $user) {
                              $url = Url::to(['user/view', 'id' => $user->getAttribute('id')]);
                              return Html::a('<span>View</span>', $url, ['title' => 'view', 'class' => 'btn btn-primary']);
                          },
                          'leadEdit' => function ($url, $user) {
                              $url = Url::to(['user/edit', 'id' => $user->getAttribute('id')]);
                              return Html::a('<span>Edit</span>', $url, ['title' => 'update', 'class' => 'btn btn-info']);
                          },
                          'leadDelete' => function ($url, $user) {
                              $url = Url::to(['user/delete', 'id' => $user->getAttribute('id')]);
                              return Html::a('<span>Delete</span>', $url, [
                                  'title' => 'delete',
                                  'class' => 'btn btn-danger',
                                  'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this user?'),
                                  'data-method' => 'post',
                              ]);
                          },
                  ]],
                ],
              ]); ?>

            <?php Pjax::end(); ?>

        </div>
      </div>

  </div>
</div>
  <div class="row">
      <div class="col-lg-12">
        <?= Html::a('<span>Back</span>', '/', ['class' => 'btn btn-default']);?>
      </div>
  </div>

