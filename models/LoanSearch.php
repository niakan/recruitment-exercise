<?php

namespace app\models;

use Yii;
use yii\base\Model;

class LoanSearch extends Loan
{
    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['id', 'campaign', 'duration'], 'integer'],
            [[ 'amount', 'interest'], 'number'],
            [['status'], 'boolean'],
            [['start_date', 'end_date'], 'date', 'format' => 'yyyy-mm-dd']
        ];
    }

    public function search($query, $params)
    {
        if (!($this->load($params) && $this->validate())) {
            return $query;
        }

        // queries
        return $query->andFilterWhere(['id' => $this->id])
                    ->andFilterWhere(['amount' => $this->amount])
                    ->andFilterWhere(['duration' => $this->duration])
                    ->andFilterWhere(['campaign' => $this->campaign])
                    ->andFilterWhere(['interest' => $this->interest])
                    ->andFilterWhere(['start_date' => $this->start_date])
                    ->andFilterWhere(['end_date' => $this->end_date])
                    ->andFilterWhere(['status' => $this->status]);
    }
}