<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UserSearch extends User
{    
    public function rules()
    {
        // only fields in rules() are searchable
        return [
            [['id', 'first_name', 'last_name', 'email', 'lang'], 'safe'],
            [['phone', 'personal_code'], 'integer'],
            [['active', 'dead'], 'boolean'],
        ];
    }

    public function search($query, $params)
    {
        if (!($this->load($params) && $this->validate())) {
            return $query;
        }

        // queries
       return $query->andFilterWhere(['like', 'first_name', $this->first_name])
                    ->andFilterWhere(['like', 'last_name', $this->last_name])
                    ->andFilterWhere(['like', 'email', $this->email])
                    ->andFilterWhere(['personal_code' => $this->personal_code])
                    ->andFilterWhere(['id' => $this->id])
                    ->andFilterWhere(['lang' => $this->lang])
                    ->andFilterWhere(['dead' => $this->dead])
                    ->andFilterWhere(['active' => $this->active]);
    }
}