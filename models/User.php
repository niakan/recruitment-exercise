<?php

namespace app\models;

use Yii;
use yii\data\Sort;
use app\models\Loan;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool $active
 * @property bool $dead
 * @property string $lang
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['personal_code', 'phone'], 'default', 'value' => null],
            [['personal_code', 'phone'], 'integer'],
            [['active', 'dead'], 'boolean'],
            ['email', 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
            'age' => 'Age',
        ];
    }

    /**
     * Calculation method to get user age by personal code
     * 
     * @return integer
     */
    public static function getUserAge($personalCode)
    {
        if (!preg_match('/^\d{11}$/', $personalCode)) {
            return 0;
        }

        $centry = substr($personalCode, 0, 1);
        $year = substr($personalCode, 1, 2);
        $month = substr($personalCode, 3, 2);
        $day = substr($personalCode, 5, 2);

        switch ($centry) {
            case 1:
            case 2:
                $year = '18'.$year;
            case 3:
            case 4:
                $year = '19'.$year;
            case 5:
            case 6:
                $year = '20'.$year;
        }
        
        $birthDate = new \Datetime($year .'-' . $month . '-' . $day);

        $date = new \Datetime();
        
        return $date->diff($birthDate)->y ?? 0;
    }

    /**
     * Check user age is under limitation for a loan or not
     * 
     * @return boolean
     */
    public static function checkUserAgeForLoan($personalCode)
    {
        return self::getUserAge($personalCode) > Loan::AGE_LIMITATION ? true : false;
    }
}
