<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use app\models\UserSearch;

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Display all users.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $user = new User;
        $params = Yii::$app->request->queryParams;
        
        $query = $user->find();

        $searchModel = new UserSearch;
        $query = $searchModel->search($query, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);

        return $this->render('index',['listDataProvider' => $dataProvider,
                                      'searchModel' => $searchModel,
                                      'user' => $user]);
    }

    /**
     * Display user by id.
     * 
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(['id' => $id]),
        ]);

        return $this->render('view', ['dataProvider' => $dataProvider]);
    }

    /**
     * Eidt user by id.
     *
     * @param $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $user = User::findOne(['id' => $id]);

        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->save();
            $this->redirect(["user/view/{$user->id}"]);
        }

        return $this->render('edit', ['user' => $user, 'error' => $user->errors]);
    }

    /**
     * Create user.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $user = new User;
        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $lastId = $user->find()->orderBy(['id' => SORT_DESC])->one()->id;
            $user->id = ++$lastId;
            $user->save();
            $this->redirect(["user/view/{$user->id}"]);
        }

        return $this->render('add', ['user' => $user, 'error' => $user->errors]);
    }

    /**
     * Delete user by id.
     *
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = User::findOne(['id' => $id])->delete();

        $this->redirect(['user/index']);
    }


}
