<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Loan;
use app\models\LoanSearch;
use yii\data\ActiveDataProvider;
use yii\data\Sort;

class LoanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Display all loans.
     *
     * @return mixed
     */
    public function actionIndex()
    {   
        $loan = new Loan;
        $params = Yii::$app->request->queryParams;
        
        $query = $loan->find();

        $searchModel = new LoanSearch;
        $query = $searchModel->search($query, $params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
        ]);

        return $this->render('index', ['listDataProvider' => $dataProvider,
                                        'searchModel' => $searchModel,
                                        'loan' => $loan]);
    }

    /**
     * Display loan by id.
     * 
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Loan::find()->where(['id' => $id]),
        ]);

        return $this->render('view', ['dataProvider' => $dataProvider]);
    }

    /**
     * Eidt loan by id.
     * 
     * @param $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $loan = Loan::findOne(['id' => $id]);
        $user = new  User;
                
        if (!$user->findOne(['id' => Yii::$app->request->post('Loan')['user_id']]) && Yii::$app->request->post()) {
            return $this->render('edit', ['loan' => $loan, 'error' => 'The specified user id cannot be found.']);
        }

        // Check age limitation
        if (!$user::checkUserAgeForLoan($dataUser->personal_code)) {
            return $this->render('add', ['loan' => $loan, 'error' => 'The user is under 18 age.']);
        }

        if ($loan->load(Yii::$app->request->post()) && $loan->validate()) {
            $loan->save();
            $this->redirect(["loan/view/{$loan->id}"]);
        }

        return $this->render('edit', ['loan' => $loan, 'error' => $loan->errors]);
    }

    /**
     * Create loan.
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $loan = new Loan;
        $user = new User;
        $user_id = null;

        if ($params = Yii::$app->request->queryParams) {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
        }

        if (!($dataUser = $user->findOne(['id' => Yii::$app->request->post('Loan')['user_id']])) && Yii::$app->request->post()) {
            return $this->render('add', ['loan' => $loan, 'error' => 'The specified user id cannot be found.']);
        }

        // Check age limitation
        if (Yii::$app->request->post() && !$user::checkUserAgeForLoan($dataUser->personal_code)) {
            return $this->render('add', ['loan' => $loan, 'error' => 'The user is under 18 age.']);
        }
        
        if ($loan->load(Yii::$app->request->post()) && $loan->validate()) {
            $lastId = $loan->find()->orderBy(['id' => SORT_DESC])->one()->id;
            $loan->id = ++$lastId;
            $loan->save();
            $this->redirect(["loan/view/{$loan->id}"]);
        }

        return $this->render('add', ['loan' => $loan, 'user_id' => $user_id, 'error' => $loan->errors]);
    }

    /**
     * Delete loan by id.
     * 
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $loan = Loan::findOne(['id' => $id])->delete();

        $this->redirect(['loan/index']);
    }


}
