<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

/**
 * This command is for seeding loan table.
 *
 * @author Shahrokh Niakan <shahrokhniakan@gmail.com>
 */
class LoanController extends Controller
{
    /**
     * Fill table with loans json file 
     */
    public function actionIndex()
    {
        // Get db config data
        $db = require __DIR__ . '/../config/db.php';
        unset($db['class']);

        // Get loans data from json file
        $loans = file_get_contents(__DIR__.'/../loans.json');
        $loans = json_decode($loans, true);

        $connection = new \yii\db\Connection($db);
        $connection->open();

        foreach ($loans as $key => $value) {
            $connection->createCommand()->insert('loan', [
                'id'         => $value['id'],
                'user_id'    => $value['user_id'],
                'amount'     => $value['amount'],
                'interest'   => $value['interest'],
                'duration'   => $value['duration'],
                'start_date' => date('Y-m-d', $value['start_date']),
                'end_date'   => date('Y-m-d', $value['end_date']),
                'campaign'   => $value['campaign'],
                'status'     => $value['status'],    
            ])->execute();
        }

        $connection->close();
        
        echo 'loan seed is done!';
    }
}
