<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

/**
 * This command is for seeding user table.
 *
 * @author Shahrokh Niakan <shahrokhniakan@gmail.com>
 */
class UserController extends Controller
{
    /**
     * Fill table with users json file 
     */
    public function actionIndex()
    {
        // Get db config data
        $db = require __DIR__ . '/../config/db.php';
        unset($db['class']);

        // Get users data from json file
        $users = file_get_contents(__DIR__.'/../users.json');
        $users = json_decode($users, true);

        $connection = new \yii\db\Connection($db);
        $connection->open();

        foreach ($users as $key => $value) {
            $connection->createCommand()->insert('user', [
                'id'            => $value['id'],
                'first_name'    => $value['first_name'],
                'last_name'     => $value['last_name'],
                'email'         => $value['email'],
                'personal_code' => $value['personal_code'],
                'phone'         => $value['phone'],
                'active'        => $value['active'],
                'dead'          => $value['dead'],
                'lang'          => $value['lang'],    
            ])->execute();
        }

        $connection->close();
        
        echo 'user seed is done!';
    }
}
