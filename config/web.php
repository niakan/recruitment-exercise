<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'jg_Paid4ARVT4sm3MQvZyO-FgjiRK-kh',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'pattern'   => 'user/view/<id:\d+>',
                    'route'     => 'user/view',
                    'defaults'  => ['id' => 1],
                ],
                [
                    'pattern'   => 'user/edit/<id:\d+>',
                    'route'     => 'user/edit',
                    'defaults'  => ['id' => 1],
                ],
                [
                    'pattern'   => 'user/add',
                    'route'     => 'user/add',
                ],
                [
                    'pattern'   => 'user/delete/<id:\d+>',
                    'route'     => 'user/delete',
                    'defaults'  => ['id' => 1],
                ],
                [
                    'pattern'   => 'loan/view/<id:\d+>',
                    'route'     => 'loan/view',
                    'defaults'  => ['id' => 1],
                ],
                [
                    'pattern'   => 'loan/edit/<id:\d+>',
                    'route'     => 'loan/edit',
                    'defaults'  => ['id' => 1],
                ],
                [
                    'pattern'   => 'loan/add',
                    'route'     => 'loan/add',
                ],
                [
                    'pattern'   => 'loan/delete/<id:\d+>',
                    'route'     => 'loan/delete',
                    'defaults'  => ['id' => 1],
                ],      
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
