<?php

use app\models\User;
use Yii;

const TEST_PERSONAL_CODE = '39311110433';

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    // tests
    public function testUserAge()
    {
        $trueBirthDate = new \Datetime('93-11-11');

        $falseBirthDate = new \Datetime('92-11-11');

        $age  = User::getUserAge(TEST_PERSONAL_CODE);

        $date = new \Datetime();

        $this->assertEquals($date->diff($trueBirthDate)->y, $age);

        $this->assertNotEquals($date->diff($falseBirthDate)->y, $age);
    }

    // tests
    public function testUserAgeLimitation()
    {
        $this->assertTrue(User::checkUserAgeForLoan(TEST_PERSONAL_CODE));
    }

}